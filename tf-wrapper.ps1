
# VPC Variables
$GitPath            = Read-host -Prompt "Enter the path to the Terraform git template"
$user               = $env:username
$EnvironmentName    = Read-Host -Prompt "Enter the name for the new environment to be deployed"
$TFworkspace        = "$user-$environmentName"
$VPCName            = "$TFWorkspace-VPC"
$IGWName            = "$TFWorkspace-IGW"
$AWSRegion          = "eu-west-2"
$CIDR               = Read-Host -Prompt "Enter the CIDR ramge to be used for the new VPC (example 10.220.24/21)"
$PrivateSubnet1CIDR = Read-Host -Prompt "Enter the Subnet address for the 1st Private Subnet (in CIDR format)"
$PublicSubnet1CIDR  = Read-Host -Prompt "Enter the Subnet address for the 1st Public Subnet (in CIDR format)"
$PrivateSubnet2CIDR = Read-Host -Prompt "Enter the Subnet address for the 2nd Private Subnet (in CIDR format)"
$PublicSubnet2CIDR  = Read-Host -Prompt "Enter the Subnet address for the 2nd Public Subnet (in CIDR format)"

# Instance Stack Variables
$KeyName            = "london-pair"
$AMIOwnerID         = "477661052598"
$WebEC2Type         = "t2.small"
$WebEC2Count        =  "1"
#$web_subnet         = 
$AppEC2Type         = "t2.small"
$AppEC2Count        = "1"
#$App_subnet         =
$DBEC2Type          = "t2.medium"
$DBEC2Count         = "1"
#$DB_subnet          =
#variable "web_security_group" {
 # type = "list"
#}
#variable "app_security_group" {
 # type = "list"
#}

#variable "db_security_group" {
 # type = "list"
  
#}

# Terraform Commands
# Change to the specified GitPath
CD $GitPath

############## NEED OUTPUTS ADDING TO TEMPLATE FILE ############################



# Create new Workspace for the environment being created (this will create a new tfstate file int he S3 bucket. 
Terraform workspace new $TFWorkspace

Terraform init

#Create the Terraform plan

Terraform plan -var "environment=$environmentName" -var "name=$vpcName" -var "cidr=$cidr" -var "public1=$Publicsubnet1CIDR" -var "public2=$Publicsubnet2CIDR" -var "private1=$PrivateSubnet1CIDR" -var "private2=$PrivateSubnet2CIDR" -var "region=$AWSRegion" -var "gatewayname=$IGWName" -out=c:\temp\$TFworkspace

############# Display plan in easier format to read #####################

############# Display Outputs in list view??? #####################


# Apply the Terraform Plan - Yes/No 
[ValidateSet('Yes','No')]$Apply = Read-Host -Prompt "Do you wish to apply the terraform template (yes/no)"

If ($apply -eq 'yes') {
    terraform apply "c:\temp\$TFworkspace"
}
############# Export Outputs into a csv???? #####################








